# The order of creating new feature in the project

- Create new branch with proper name (ie: **writing-to-file-impl**) based on branch **main**
- Implements feature into few commits with parameters <code>--amend</code> as last one
- Some new additional code not related directly with this feature should be committed on additional branch
  with under similar name ie: writing-to-file-impl--documentation 
- After all do push on proper origin branch <code>what is the proper code?</code>

Useful git feature are:
- <code>git stash save "message"</code> and later <code>got pop</code>
- <code>git stash list</code> 

