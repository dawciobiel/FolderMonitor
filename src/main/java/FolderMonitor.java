import calendar.CalendarUtils;
import config.ConfigUtils;
import files.FileOperationException;
import files.FileUtils;
import gson.GsonUtils;
import language.LanguageBundle;
import lombok.Data;
import model.AttributesHolder;
import model.AttributesHolderAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.function.Consumer;

import static calendar.CalendarUtils.isDateIsEven;

/**
 *
 */
@Data
public class FolderMonitor {

    private static final Logger logger = LogManager.getLogger(FolderMonitor.class);

    private static final Marker APP_MARKER = MarkerManager.getMarker("application");

    private static final String JAR = "jar";
    private static final String XML = "xml";

    /*
    ConcurrentLinkedQueue
    PriorityBlockingQueue
    LinkedBlockingQueue

    No need to use volatile
     */
    /**
     * PriorityBlockingQueue - Thread safe queue implementation
     *
     * <p>PriorityBlockingQueue:</p>
     * <br />
     * <p>This is the unbounded blocking queue with priority. Every time you leave the queue, you will return the elements with the highest or lowest priority (here the rules can be made by yourself). The internal is implemented by using the balanced binary tree, and the traversal does not guarantee the order;</p>
     */
    public Queue<AttributesHolder> holders = new PriorityBlockingQueue<>();

    /**
     * Holding number of files with extensions what was moved from one folder to another
     */
    private Map<String, BigInteger> extMap = new HashMap<>();


    public void doFolderMonitoring() {
        try {
            String home = ConfigUtils.readConfigValue("FOLDER_HOME");
            logger.info(LanguageBundle.getResource("WATCHING_DIRECTORY_FOR_CHANGES"), home);
            // Get the path of the monitoringFolder which you want to monitor.
            Path monitoringFolder = Path.of(home);

            // Create a watch service
            WatchService watchService = FileSystems.getDefault().newWatchService();

            // Register the monitoringFolder with the watch service
            WatchKey watchKey = monitoringFolder.register(watchService,
                    StandardWatchEventKinds.ENTRY_CREATE);

            // Poll for events
            while (true) {
                for (WatchEvent<?> event : watchKey.pollEvents()) {
                    watchingFolderEvent(monitoringFolder, event);
                }

                // Reset the watch key everytime for continuing to use it for further event polling
                boolean valid = watchKey.reset();
                if (!valid) {
                    break;
                }

            }
        } catch (IOException | FileOperationException e) {
            logger.error(LanguageBundle.getResource("EXCEPTION_MESSAGE"), e.getMessage());
            e.printStackTrace();
        }
    }

    private void watchingFolderEvent(Path monitoringFolder, WatchEvent<?> event) throws FileOperationException {
        // Get file name from even context
        WatchEvent<Path> pathEvent = (WatchEvent<Path>) event;
        Path contextFileName = pathEvent.context();

        // Get full path with file name
        Path fullPathWithFileName = Path.of(monitoringFolder.toString(), contextFileName.toString());

        // Check type of event
        WatchEvent.Kind<?> kind = event.kind();

        // Perform necessary action with the event
        if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
            logger.info(LanguageBundle.getResource("NEW_FILE_IS_CREATED"), contextFileName);
            holders.add(createHolder(fullPathWithFileName));
        }
    }

    /**
     * Create {@link model.AttributesHolder} based on {@link java.nio.file.Path} file name and creation date time
     *
     * @param fullPathWithFileName {@link java.nio.file.Path} File name
     * @return {@link model.AttributesHolder} object
     * @throws {@link files.FileOperationException} description of exception thrown
     */
    private AttributesHolder createHolder(Path fullPathWithFileName) throws FileOperationException {
        String fileExt = FileUtils.getExtensionByApacheCommonLib(fullPathWithFileName.toString());
        FileTime creationDateZULU, lastAccessDateZULU, modifiedTimeDateZULU;

        try {
            BasicFileAttributes basicFileAttributes = FileUtils.getFileDates(fullPathWithFileName);

            creationDateZULU = basicFileAttributes.creationTime();
            lastAccessDateZULU = basicFileAttributes.lastAccessTime();
            modifiedTimeDateZULU = basicFileAttributes.lastModifiedTime();
        } catch (IOException e) {
            logger.error(LanguageBundle.getResource("READING_FILE_ATTRIBUTES_NOT_POSSIBLE"), fullPathWithFileName);
            throw new FileOperationException(e.toString());
        }

        Date creationDateCEST = CalendarUtils.convertDateToUTC(creationDateZULU);
        Date lastAccessDateCEST = CalendarUtils.convertDateToUTC(lastAccessDateZULU);
        Date modifiedTimeDateCEST = CalendarUtils.convertDateToUTC(modifiedTimeDateZULU);

        Path sourceFolder = Path.of(ConfigUtils.getResource("FOLDER_HOME"));
        // Selecting what destination folder it's going to be based on creation date
        Path destinationFolder = Path.of(selectTheCorrectDestinationFolder(
                fileExt.toLowerCase(Locale.getDefault()),
                isDateIsEven(creationDateCEST))
        );

        AttributesHolder attributesHolder = new AttributesHolder(
                AttributesHolderAction.MOVE,
                fullPathWithFileName,
                fileExt,
                creationDateCEST,
                lastAccessDateCEST,
                modifiedTimeDateCEST,
                sourceFolder,
                destinationFolder);

        logger.debug(LanguageBundle.getResource("FILE_ATTRIBUTES"), attributesHolder);

        return attributesHolder;
    }

    /**
     * Based of config file this method will return proper destination folder based on file extension and creation date file parity
     *
     * @param fileExtension as {@link java.lang.String} object
     * @return Name of destination folder as {@link java.lang.String} object
     */
    private String selectTheCorrectDestinationFolder(String fileExtension, boolean parity) {
        String destination;

        switch (fileExtension) {
            case JAR -> {
                if (parity) {
                    destination = ConfigUtils.readConfigValue("FOLDER_DEV");
                } else {
                    destination = ConfigUtils.readConfigValue("FOLDER_TEST");
                }

            }
            case XML -> destination = ConfigUtils.readConfigValue("FOLDER_DEV");
            default -> destination = ConfigUtils.readConfigValue("FOLDER_HOME");
        }

        return destination;
    }

    /**
     * Proceed collection of holders.
     * Add result to json (by Gson)
     */
    public void proceedHolders() {
        while (true) {

            boolean fileMoved = false;
            AttributesHolder holder = holders.poll();
            if (holder != null) {
                if (!StringUtils.equals(holder.getSource().toString(), holder.getDestination().toString())) {
                    fileMoved = FileUtils.moveFile(holder);
                }

                if (fileMoved) {
                    BigInteger valueInMap = extMap.get(holder.getExtension());
                    BigInteger numberOfMovedFilesWithThisExt = valueInMap == null ? BigInteger.ZERO : valueInMap;
                    extMap.put(holder.getExtension(), numberOfMovedFilesWithThisExt.add(BigInteger.ONE));

                    try {
                        GsonUtils.createNewJsonFile(generateDTO(extMap));
                    } catch (FileOperationException ex) {
                        /* todo log error maybe with stacktrace:
                                1. Should application terminated?
                          */
                    }

                }
            }
        }
    }

    private String generateDTO(Map<String, BigInteger> map) {
        StringBuilder sb = new StringBuilder("{");
        map.forEach((k, v) -> sb.append('"' + k + '"' + ":" + v));
        sb.append("}");

        return sb.toString();
    }

    /**
     * The method creates folders where the application works
     */
    public void createFolders() {
        List<String> paths = Arrays.asList(
                ConfigUtils.readConfigValue("FOLDER_DEV"),
                ConfigUtils.readConfigValue("FOLDER_TEST"),
                ConfigUtils.readConfigValue("FOLDER_HOME")
        );

        Consumer<String> consumer = p -> {
            File file = new File(p);
            boolean isFolderCreated = file.mkdirs();
            if (!isFolderCreated) {

                boolean isFoldersAlreadyExist = file.exists();
                if (!isFoldersAlreadyExist) {
                    logger.error(LanguageBundle.getResource("ERROR_CANT_CREATE_FOLDER"), file.getName());
                    logger.error(APP_MARKER, LanguageBundle.getResource("APPLICATION_TERMINATED"));
                    System.exit(App.EXIT_STATUS_ERROR__BY_CREATING_FOLDER);
                } else {
                    logger.debug(LanguageBundle.getResource("FOLDER_ALREADY_EXIST"), file.getName());
                }
            } else {
                logger.debug(LanguageBundle.getResource("FOLDER_CREATED"), file.getName());
            }
        };
        paths.forEach(consumer);
    }

}
