package gson;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@NoArgsConstructor
public class JsonObjectDTO { // todo Rename this class

    public BigInteger filesInHOME = BigInteger.ZERO;
    public BigInteger filesInDEV = BigInteger.ZERO;
    public BigInteger filesInTEST = BigInteger.ZERO;

}
