package gson;

import com.google.gson.Gson;
import config.ConfigUtils;
import files.FileOperationException;
import language.LanguageBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileWriter;
import java.io.IOException;

/**
 * The class enables data write operations to a JSON file using the Gson library
 */
final public class GsonUtils {

    private static final Logger logger = LogManager.getLogger(GsonUtils.class);

    private static final Gson gson = new Gson();

    private GsonUtils() {
    }

    /**
     * Create and/or write data to json file
     *
     * @param data Data object to be written
     * @throws FileOperationException if the named file exists but is a directory rather than a regular file, does not exist but cannot be created, or cannot be opened for any other reason
     */
    public static void createNewJsonFile(Object data) throws FileOperationException {
        try {
            // todo Buffer creating output file or just create it when application finishing working
            //      FileWriter.writeBuffer can be -true-
            String outputFile = ConfigUtils.getResource("OUTPUT_FILE");
            FileWriter fw = new FileWriter(outputFile);
            gson.toJson(data, fw);
        } catch (IOException e) {
            logger.error(LanguageBundle.getResource("CANT_CREATE_OR_WRITE_JSON_FILE"), e.toString());
            throw new FileOperationException(e.getMessage());
        }
    }

}
